# Statistiques sur les mots les plus recherchés dans le Dictionnaire Electronique des Synonymes (DES) en 2019 et 2020

Ce dépôt est en relation avec la publication [Usages du Dictionnaire Électronique des Synonymes (DES) du CRISCO](https://halshs.archives-ouvertes.fr/halshs-03266189) et avec l'article de blog ["Usages du Dictionnaire Électronique des Synonymes (DES) du CRISCO : focus sur les mots inexistants"](https://mrsh.hypotheses.org/5578) dans [le carnet de la MRSH](https://mrsh.hypotheses.org)

## Mode d'emploi des versions interactives au format html

### Contenu du dépôt

Les graphiques ont été générés à partir de la librairie [plotly express](https://plotly.com/python/plotly-express/) en python.

Quatre graphiques ont été créés sur le même principe (avec des histogrammes et sur l'axe X les jours de l'année ):

- [GraphiquesRequetesMotsRecherches2019.html ](https://crisco4.unicaen.fr/DESMotsRecherches/GraphiquesRequetesMotsRecherches2019.html) (Figures 1, 10  et 12)

- [GraphiquesRequetesMotsRecherches2020.html](https://crisco4.unicaen.fr/DESMotsRecherches/GraphiquesRequetesMotsRecherches2020.html) (Figures 2 et 13)

- [GraphiquesMotsRecherches2019.html](https://crisco4.unicaen.fr/DESMotsRecherches/GraphiquesMotsRecherches2019.html) (Figure 3)

- [GraphiquesMotsRecherches2020.html](https://crisco4.unicaen.fr/DESMotsRecherches/GraphiquesMotsRecherches2020.html) (Figure 4)

Le graphique [GraphiquesStatMots&ReqRecherches2019-20.html](https://crisco4.unicaen.fr/DESMotsRecherches/GraphiquesStatMots&ReqRecherches2019-20.html) qui comprend :

- 1. Moyenne par jour pour chaque mois des années 2019 et 2020 du nombre de mots différents demandés et du nombre de requêtes (Figures 5, 6 et 7 de l'article associé)
  2. Ecart-type par jour pour chaque mois pour les années 2019 et 2020 du nombre de mots différents demandés et du nombre de requêtes (Figures 8 et 9 et 11)
  3. Le nombre de requêtes par jour par mot en moyenne (Figure 14)

Le graphique [GraphiquesStatMotsFrequence.html](https://crisco4.unicaen.fr/DESMotsRecherches/GraphiquesStatMotsFrequence.html) qui comprend (Figures 15 et 16) :

- Nombre de mots valides et différents les plus recherchés et leur nombre total de requêtes par mois 

- Nombre de requêtes selon le partage en 10 groupes égaux avec la moyenne et la médiane

Le graphique [Figure17-wordcloud.png](https://crisco4.unicaen.fr/DESMotsRecherches/Figure17-wordcloud.png) (Figure 19) qui donne les 1000 premiers mots recherchés dans le DES en 2019 et 2020

Le graphique [MotsLesPlusRecherchesGrapheAdjacence-1000.jpg](https://crisco4.unicaen.fr/DESMotsRecherches/MotsLesPlusRecherchesGrapheAdjacence-1000.jpg) (Figures 20 à 24) qui donne une vue d'ensemble du graphe d'adjacence des 1000 premiers mots recherchés.

### Comment optimiser l'exploitation des graphiques

Une fois le fichier html chargé dans votre navigateur, le graphique apparait au centre.

- le passage de la souris sur l'histogramme affiche une fenêtre avec la date, la nature des données survolées (requêtes valides, variantes ou requêtes invalides et le nombre de requêtes par exemple).

- Le nombre de requêtes ou de mots sont classées en 3 catégories:
  
  > * en bleu foncé, les requêtes de mots valides ( = existants dans le DES)
  > * en bleu moyen, les requêtes des variantes ( par exemple des mots sans accents ceder -> céder dépecher -> dépêcher ou des erreurs courantes : aporter -> apporter )
  > * en bleu clair, les requêtes de mots invalides (qui peuvent être générés ou saisis de façon incorrecte)

- les traits verticaux de couleur magenta correspondent au dimanche de chaque semaine.

- dans la légende, à droite,  avec les rectangles en bleu foncé, bleu dur et bleu clair, vous pouvez décider d'afficher ou pas chacune des catégories (requêtes valides, variantes ou requêtes invalides) en cliquant dessus. Par exemple en cliquant sur requêtes invalides dans la légende, la case devient plus opaque et l'axe Y est réactualisé pour afficher au mieux les données restantes (les requêtes valides et les variantes).

A droite, la légende  et le menu général suivant s'affiche en haut à droite :

![menu de plotly](Menu1.png "Menu général plotly")

**Conseils pour naviguer dans les graphiques :**

- dans la légende, à droite, vous pouvez décider d'afficher ou
   pas chacune des catégories (requêtes valides, variantes ou requêtes 
  invalides) en cliquant dessus. Par exemple en cliquant sur requêtes 
  invalides dans la légende, la case devient plus opaque et l'axe Y est 
  réactualisé pour afficher au mieux les données restantes (les requêtes 
  valides et les variantes).
- dans le menu en haut à droite, en mode zoom ![loading-ag-533](zoom.jpg) 
   (mode par défaut), vous pouvez sélectionner une partie du graphique, 
  par exemple les jours du mois de septembre, avec le bouton gauche de la 
  souris. Le graphe est automatiquement réactualisé sur la zone 
  sélectionnée. Etant plus précis, les chiffres pour chacune des 
  catégories s'affichent. Par exemple le dimanche 1er septembre, nous 
  avons 200.000 (200k) de requêtes valides, 12k de requêtes avec des 
  variantes et 30k de requêtes sur des mots invalides.
- le passage de la souris sur l'histogramme affiche une fenêtre avec la date,
   la nature des données survolées (requêtes valides, variantes ou 
  requêtes invalides par exemple pour le premier graphique) et le nombre.

Pour revenir à la présentation de base, il suffit de cliquer sur le bouton "autoscale" ![loading-ag-535](autoscale.jpg)

- les traits verticaux de couleur magenta correspondent au dimanche de chaque semaine.
- d'autres fonctions dans le menu en haut à droite et affiché ci-dessous vous propose aussi de vous déplacer ![](deplacement.jpg)dans le graphique ou prendre une photo  ![](photo.jpg)

## Versions interactives des trois histogrammes de l'article de blog

- [HistoFreqMotsInexistants2019.html](https://crisco4.unicaen.fr/DESMotsRecherches/HistoFreqMotsInexistants2019.html) 

- [HistoFreqMotsInexistants2020.html](https://crisco4.unicaen.fr/DESMotsRecherches/HistoFreqMotsInexistants2020.html) 

- [HistoFreqMotsInexistants2021.html](https://crisco4.unicaen.fr/DESMotsRecherches/HistoFreqMotsInexistants2021.html) 
